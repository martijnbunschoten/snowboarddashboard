import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import axios from 'axios';


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    width: '100%',
    background: 'lightgrey'
    
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

const useStyless = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    backgroundColor: 'grey',
    color:'black',
  },
}));

const ButtonStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));

  const Checkboxes = () => {
  const classes = useStyles();
  const classes2 = useStyless();
  const classes3 = ButtonStyles();
  const [state, setState] = React.useState({
    trick1: true,
    trick2: false,
    trick3: false,
    trick4: false,
    trick5: false,
    trick6: false,
    trick7: false,
    trick8: false,
    trick9: false,
    trick10: false,
    trick11: false,
    trick12: false,
    trick13: false,
    trick14: false,
    trick15: false
  });

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    let arr = [];
    for (let key in state) {
      if(state[key] === true) {
        arr.push(key);
      }
    }
    let data = {
      check: arr.toString()
    };
    axios.post('http://localhost:4000/checks/add', data)
          .then(res => console.log(res.data));
    console.log(arr);
}


  const { trick1, trick2, trick3, trick4, trick5, trick6, trick7, trick8, trick9, trick10,
  trick11, trick12, trick13, trick14, trick15 } = state;

  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Jibbing</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={trick1} onChange={handleChange('trick1')} value="gilad" />}
            label="50-50"
          />
          <FormControlLabel
            control={<Checkbox checked={trick2} onChange={handleChange('trick2')} value="jason" />}
            label="Frontside boardslide"
          />
          <FormControlLabel
            control={
              <Checkbox checked={trick3} onChange={handleChange('trick3')} value="antoine" />
            }
            label="Backside boardslide"
          />
           <FormControlLabel
            control={
              <Checkbox checked={trick4} onChange={handleChange('trick4')} value="antoine" />
            }
            label="Frontside Tailslide"
          />
           <FormControlLabel
            control={
              <Checkbox checked={trick5} onChange={handleChange('trick5')} value="antoine" />
            }
            label="Backside Tailslide"
          />
        </FormGroup>
      </FormControl>
      <FormControl component="fieldset" className={classes.formControl}>
      <FormLabel component="legend">Buttering</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={trick6} onChange={handleChange('trick6')} value="gilad" />}
            label="Tailpress"
          />
          <FormControlLabel
            control={<Checkbox checked={trick7} onChange={handleChange('trick7')} value="jason" />}
            label="Nosepress"
          />
          <FormControlLabel
            control={
              <Checkbox checked={trick8} onChange={handleChange('trick8')} value="antoine" />
            }
            label="Frontside 360 Noseroll"
          />
          <FormControlLabel
            control={
              <Checkbox checked={trick9} onChange={handleChange('trick9')} value="antoine" />
            }
            label="Backside 360 Noseroll"
          />
          <FormControlLabel
            control={
              <Checkbox checked={trick10} onChange={handleChange('trick10')} value="antoine" />
            }
            label="Tailslide 180"
          />
        </FormGroup>
      </FormControl>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Air Tricks</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={trick11} onChange={handleChange('trick11')} value="Ollie" />}
            label="Ollie"
          />
          <FormControlLabel
            control={<Checkbox checked={trick12} onChange={handleChange('trick12')} value="Backside 180" />}
            label="Backside 180"
          />
          <FormControlLabel
            control={
              <Checkbox checked={trick13} onChange={handleChange('trick13')} value="antoine" />
            }
            label="Frontside 180"
          />
          <FormControlLabel
            control={<Checkbox checked={trick14} onChange={handleChange('trick14')} value="jason" />}
            label="Backside 360"
          />
          <FormControlLabel
            control={<Checkbox checked={trick15} onChange={handleChange('trick15')} value="jason" />}
            label="frontside 360"
          />
        </FormGroup>
      </FormControl>
    {/* <Paper className={classes2.root}>
      <Typography variant="h5" component="h3">
      </Typography> */}
      {/* <Typography component="p">
      <Button onClick={submitHandler} variant="contained" className={classes3.button}>
          Generate my charts
        </Button>
      </Typography> */}
    {/* </Paper> */}
    </div>
    
  );
}

export default Checkboxes;