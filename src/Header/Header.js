import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { FaSnowboarding } from 'react-icons/fa';
import '../App.css';


const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    backgroundColor: 'red',
    color:'white',
    fontWeight: 'bold'
  },
}));

 const Header = () => {

  const classes = useStyles();

  return (
    <div>
      
      <Paper className={classes.root}>
        <Typography variant="h5" component="h3">
          Improve your snowboard skills!
          <FaSnowboarding size="80px" className="icon" />
        </Typography>
        <Typography component="p">
          By logging your progress allowing you to progress faster.
        </Typography>
      </Paper>
    </div>
  );
  }

export default Header;