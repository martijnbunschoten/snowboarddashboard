import React from 'react';
import { Component } from 'react';
import './App.css';
import Checkboxes from './checkbox/Checkboxes';
import BarCharts from './charts/Charts';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ClippedDrawer from './drawer/drawer';

class App extends Component {
  render() {
    return (
     <div className="App">
       <ClippedDrawer />
     </div>
    );
  }
}

export default App;
