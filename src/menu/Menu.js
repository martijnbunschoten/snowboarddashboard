import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { FaSnowboarding } from 'react-icons/fa';
import '../App.css';
// import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: 'red',
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    float: 'left',
    display: 'flex'
  },
  MuiAppBarcolorPrimary: {
    backgroundColor: 'red',
  }
}));

export default function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar className='menu'position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="red" aria-label="menu">
          <FaSnowboarding size="30px" className="icon" />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Improve your snowboarding
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}